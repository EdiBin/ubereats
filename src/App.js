/* eslint-disable jsx-a11y/alt-text */
import React, {useState,useEffect} from 'react';
import header from './components/header.js'
import mainBanner from './components/mainBanner'
import useHsm  from './components/horizontalScrollMenu'
import staticBanner from './components/staticBanner'
import cities from './components/cities'
import categories from './components/categories'
import footer from './components/footer'
import './App.css';

function useApp() {
  const [onScroll, setOnScroll] = useState(false)
  useEffect(()=>{
    window.addEventListener('scroll', ()=>{(window.scrollY>330 ? setOnScroll(true) : setOnScroll(false))})
  })
  return (
    <div className="App">
      {header(onScroll)}
      {mainBanner()}
      <div className='body'>
      {useHsm('Food Delivery in San Francisco Bay Area',[1,2,3,4,5,6])}
      <hr/>
      {staticBanner()}
      <hr/>
      {useHsm('New on Uber Eats',[1,2])}
      <hr/>
      {cities()}
      <hr/>
      {categories()}
      </div>
      {footer()}
    </div>
  );
}

export default useApp;
