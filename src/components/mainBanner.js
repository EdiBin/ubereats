/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import './mainBanner.css'
import serach from './search';
const mainBanner = () =>{
  return(
    <div className='mainBanner'>
    <div className='mainBanner-sidePhoto'>
      <img src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/317b14d7b051b7e496c9f1ba4db156b4.svg" class="sidePhoto left"/>
      <img src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/2d72424dcc82f116a7d8850d1f44ec09.svg" class="sidePhoto right"/>
    </div>
    <div class='mainBanner-content'>
      <h1 class='mainBanner-content-text'>Restaurants you love, delivered to you</h1>
      <form class='mainBanner-content-form'>
        {serach()}
        <button class='mainBanner-content-form-button'><b>Find Food</b></button>
      </form>
    </div>
  </div>
  )
}
export default mainBanner