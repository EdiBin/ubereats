import React from 'react'
import search from './search'
import classNames from 'classnames'
import './header.css'
const header = (onScroll) =>{
  return(
    <div className='header'>
      <div className=''>
        <img src='https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/f8f0721f871b3704cce92eb96bc6e504.svg' alt='ubereats'/>
      </div>
      <div className={classNames('searchBar')}>
        {search()}
        <div className='searchBar-scrolled' style={onScroll?{transform: 'scaleX(0)'}:{transform: 'scaleX(1)'}}/>
      </div>
      <div>
        <a href='/'>
          Sign In
        </a>
      </div>
    </div>
  )
}
export default header