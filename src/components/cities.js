import React from 'react'
import './cities.css'
const cities = () => {
  return (
    <React.Fragment>
      <div class='c-header'>
      <h2>Cities Near You</h2>
      <a href='/'>View all 500+ cities</a>
      </div>
      <div class='c-cities'>
        <a class="dt du" href="/en-US/atlanta/">
        <div class="c-textStyle">Atlanta</div>
        </a>
        <a class="dt dv" href="/en-US/austin/">
        <div class="c-textStyle">Austin</div>
        </a>
        <a class="dt dw" href="/en-US/baltimore/">
        <div class="c-textStyle">Baltimore-Maryland</div>
        </a><a class="dt dx" href="/en-US/boston/">
        <div class="c-textStyle">Boston</div>
        </a>
        <a class="dt dy" href="/en-US/charlotte/">
        <div class="c-textStyle">Charlotte</div>
        </a>
        <a class="dt dz" href="/en-US/chicago/">
        <div class="c-textStyle">Chicago</div>
        </a>
        <a class="e0 du" href="/en-US/dallas/">
        <div class="c-textStyle">Dallas-Fort Worth</div>
        </a>
        <a class="e0 dv" href="/en-US/denver/">
        <div class="c-textStyle">Denver</div>
        </a>
        <a class="e0 dw" href="/en-US/las-vegas/">
        <div class="c-textStyle">Las Vegas</div>
        </a>
        <a class="e0 dx" href="/en-US/los-angeles/">
        <div class="c-textStyle">Los Angeles</div>
        </a>
        <a class="e0 dy" href="/en-US/miami/">
        <div class="c-textStyle">Miami</div>
        </a>
        <a class="e0 dz" href="/en-US/minneapolis/">
        <div class="c-textStyle">Minneapolis - St. Paul</div>
        </a>
        <a class="e1 du" href="/en-US/new-york/">
        <div class="c-textStyle">New York City</div>
        </a>
        <a class="e1 dv" href="/en-US/orange-county/">
        <div class="c-textStyle">Orange County</div>
        </a>
        <a class="e1 dw" href="/en-US/palm-springs/">
        <div class="c-textStyle">Palm Springs</div>
        </a>
        <a class="e1 dx" href="/en-US/philadelphia/">
        <div class="c-textStyle">Philadelphia</div>
        </a>
        <a class="e1 dy" href="/en-US/phoenix/">
        <div class="c-textStyle">Phoenix</div>
        </a>
        <a class="e1 dz" href="/en-US/portland/">
        <div class="c-textStyle">Portland</div>
        </a>
        <a class="e2 du" href="/en-US/sacramento/">
        <div class="c-textStyle">Sacramento</div>
        </a>
        <a class="e2 dv" href="/en-US/san-diego/">
        <div class="c-textStyle">San Diego</div>
        </a>
        <a class="e2 dw" href="/en-US/san-francisco/">
        <div class="c-textStyle">San Francisco Bay Area</div>
        </a>
        <a class="e2 dx" href="/en-US/seattle/">
        <div class="c-textStyle">Seattle</div>
        </a>
        <a class="e2 dy" href="/en-US/tampa/">
        <div class="c-textStyle">Tampa Bay</div>
        </a>
        <a class="e2 dz" href="/en-US/washington-dc/">
        <div class="c-textStyle">Washington D.C.</div>
        </a>
      </div>
    </React.Fragment>
  )
}
export default cities