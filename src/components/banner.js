/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import './banner.css'

const banner = (title,extra,image) =>{
  return(
    <div class='banner-mainContainer'>
      <div class="banner-imageContainer">
        <img src={image || 'https://d1ralsognjng37.cloudfront.net/ed7f7ed4-2f31-4e43-a8c2-016529467a91.jpeg'} class='banner-image'/>
      </div>
      {
        title === undefined ?
        <div class="banner-textContainer">
          <div class='banner-nameTag'>McDonald's® (Potrero)</div>
          <div class='banner-categoryTag'>$ • American • Fast Food • Burgers</div>
          <div class='banner-details'>
            <div class='banner-grayBox'>20-30 min</div>
            <div class='banner-grayBox'>
              4.5
              <img src='https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/92367108b11b8ee48b6f29cb3fef2d4d.svg'/>
              (200+)
            </div>
            <div class='banner-grayBox'>$2.49 Delivery Fee</div>
          </div>
        </div> 
        : 
        <div class='banner-extra'>
          <h3>{title}</h3>
          {extra}
        </div>
      }
    </div>
  )
}
export default banner