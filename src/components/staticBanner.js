import React from 'react'
import banner from './banner'
import './staticBanner.css'
const staticBanner = () => {
  const extra1 = <p>Get the app:</p>
  const extra2 = <a href='/'>Add your restaurant</a>
  const extra3 = <a href='/'>Sign up to deliver</a>
  return (
    <div class='sb-container'>
      {banner(
        "There's more to love in the app.",
        extra1,
        "https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/816084874de4267a8e89c881db968ed2.svg",
      )}
      {banner(
        "Your restaurant, delivered",
        extra2,
        "https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/bd4c2537aa3799e345a397e7e4c9cbac.svg",
      )}
      {banner(
        "Deliver the Eats",
        extra3,
        "https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/b96045d2e2b2989e7b09200b1c40ab73.svg",
      )}
    </div>
  )
}
export default staticBanner