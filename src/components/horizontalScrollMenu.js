import React, {useState} from 'react'
import banner from './banner'
import './horizontalScrollMenu.css'
import classNames from 'classnames';

const useHsm = (title,items) =>{
  let arr = [];
  const [transform, setTransform] = useState(false)
  items.forEach(item =>{
    arr.push(<React.Fragment key={item}>{banner()}</React.Fragment>)
  })
  return(
    <div class=''>
      <div class='hsm-titleBar'>
        <h2>{title}</h2>
        {
          arr.length > 3 &&
          <div class='hsm-bottunContainer'>
          <div class='hsm-bottun' onClick={()=>setTransform(false)}>
          <svg width="24px" height="24px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.6666 13H8.19995L11.8666 18H9.53328L5.19995 12L9.53328 6H11.8666L8.19995 11H18.6666V13Z" fill="#1F1F1F"></path>
          </svg>
          </div>
          <div class='hsm-bottun' onClick={()=>setTransform(true)}>
          <svg width="24px" height="24px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.6666 13H8.19995L11.8666 18H9.53328L5.19995 12L9.53328 6H11.8666L8.19995 11H18.6666V13Z" fill="#1F1F1F" transform="rotate(180, 12, 12)"></path>
          </svg>
          </div>
        </div>
        }
      </div>
      <div class='hsm-foodContainerTop'>
      <div className={classNames('hsm-foodContainer',transform && 'hsm-transform')}>
        {arr}
      </div>
      </div>
    </div>
  )
}
export default useHsm