import React from 'react'
import './categories.css'
import classNames from 'classnames';

const category = (image, title) => {
  return (
    <div class='category-container'>
      <div class='category-image' style={{backgroundImage:`url(${image})`}} src={image}/>
      <div class='category-title'>
        {title}
      </div>
    </div>
  )
}
const categories = () => {
 return (
   <React.Fragment>
    <h2>Explore Popular Categories</h2>
    <div class='categories-container'>
     {category(
      'https://duyt4h9nfnj50.cloudfront.net/sku/a5aa9bbba0172134449b4ad48611d92b',
      'American'
     )}
     {category(
       'https://duyt4h9nfnj50.cloudfront.net/sku/128411a1b54acd3c3c4e5263e7c58e2d',
       'Burgers'
     )}
     {category(
       'https://duyt4h9nfnj50.cloudfront.net/sku/f6e04e64903c3207e68c649e24cc2f32',
       'Fast Food'
     )}
     {category(
       'https://duyt4h9nfnj50.cloudfront.net/sku/7b2a32908c050e6b07252ffcbe651e8c',
       'Mexican'
     )}
     {category(
       'https://duyt4h9nfnj50.cloudfront.net/sku/971d80f9ccce0c8eab98014650ee97eb',
       'Pizza'
     )}
     {category(
       'https://duyt4h9nfnj50.cloudfront.net/sku/062faadce31ecb80703eb7d4d273bc22',
       'Wings'
     )}
   </div>
   </React.Fragment>
 )
}
export default categories