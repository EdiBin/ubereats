/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import './footer.css'

const footer = () => {
  return(
    <div class='footer-container'>
      <div class='footer-contentTop'>
        <div class='footer-innerA'>
          <img class='footer-innerA-image' src='https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/12c47a69e1022b581a7e823e9bd45466.svg'/>
          <div>
          <img src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/d0558d91063038236b60e3ef71fdc1fd.svg" class='footer-appStore'/>
          <img src="https://d3i4yxtzktqr9n.cloudfront.net/web-eats-v2/cf6dad406fdfdcd290fd40de9008ae50.png" class='footer-appStore'/>
          </div>
        </div>
        <div class='footer-innerB'>

        </div>
        <div class='footer-innerC'>

        </div>
      </div>
      <hr class='footer-br'/>
    </div>
  )
}
export default footer